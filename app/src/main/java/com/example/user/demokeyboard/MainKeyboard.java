package com.example.user.demokeyboard;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainKeyboard extends AppCompatActivity {

    EditText text;
    public static int[] butArray = {R.id.n1, R.id.n2, R.id.n3, R.id.n4, R.id.n5,R.id.n6,R.id.n7,R.id.n8,R.id.n9,R.id.n10,R.id.n11,R.id.n12,R.id.n13,R.id.n14,R.id.n15,R.id.n16,R.id.n17,R.id.n18,R.id.n19,R.id.n20,R.id.n21,R.id.n22,R.id.n23,R.id.del,R.id.clr};
    public Button[] bt= new Button[butArray.length];
    public String [] myArray = {"A", "B", "C", "D", "E", "F", "G", "H","İ","J","K","L","M","N","O","P","S","T","U","V","Y","Z"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_keyboard);
        //Tuşlara basıldığında titreşim ekleme
        final Vibrator titresim=(Vibrator) MainKeyboard.this.getSystemService(Context.VIBRATOR_SERVICE);
        // AlertDialog alert= new AlertDialog.Builder(MainActivity.this).create();
        //final MediaPlayer buttonSound = MediaPlayer.create(this,R.raw.o);
        //Tuşlara basıldığında Click sesi ekleme

        //Yazılan text'in yazı tipi boyutu
        text=(EditText) findViewById(R.id.Text1);
        final EditText Text1 = (EditText) findViewById(R.id.Text1);
        Text1.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);

        for(int i=0;i<butArray.length;i++)
        {
            final int b=i;
            final String buttonID="d"+(i+1);

            bt[b] = (Button)findViewById(butArray[b]);
            bt[b].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    titresim.vibrate(80);
                    //buttonSound.start();
                    Toast.makeText(getApplicationContext(),myArray[b],Toast.LENGTH_SHORT).show();
                    switch(v.getId()) {
                        case R.id.n1:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n2:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n3:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n4:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n5:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n6:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n7:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n8:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n9:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n10:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n11:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n12:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n13:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n14:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n15:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;

                        case R.id.n16:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n17:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n18:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n19:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n20:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n21:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n22:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.n23:
                            Text1.setText(text.getText().insert(text.getText().length(), myArray[b]));
                            break;
                        case R.id.del:
                            Text1.setText("");
                            break;

                        case R.id.clr:
                            Text1.setText("");

                    }

                }
            });
        }

    }


}
